import matplotlib.pyplot as plt
import numpy as np
from edf import EDFFile

# Create file object
e = EDFFile('C01.edf') # the file C01.edf must be in the same
                       # directory of this code

# Load data from file
e.load()

# Extract 50 records (starting from record 360) from signal 0
signal = 0
start  = 360
end    = 410
nrRec  = end-start
records = e.signal(signal)[start:end]

# Flatten array
data = records.reshape(-1)

# Create time interval
time = np.linspace(
  start*e.recordDuration(),
  end*e.recordDuration(),
  nrRec*e.signal(0).samplesNumber()
)

# Plot data
plt.plot(time, data, label=e.signal(0).label())

# Get annotations for selected records
ann = e.annotations()[start:end]

# Add "Occhi aperti" and "Occhi chiusi" events to plot
for record in ann:
  for annotation in record:
    if "Occhi aperti" in annotation['events']:
      plt.axvline(x=annotation['onset'], color='red', label="Occhi aperti")
    if "Occhi chiusi" in annotation['events']:
      plt.axvline(x=annotation['onset'], color='green', label="Occhi chiusi")

# Add legend
plt.legend()

# Show plot
plt.show()
