import numpy as np

class EDFFile:
  """
  Class representing .edf files

  Allows to read the file and access to its header, records data and annotations
  """
  
  def _edit (func):
    def _func (self, *args, **kwargs):
      if not self.editable():
        raise Exception("File is read-only")
      else:
        func(self, *args, **kwargs)
    _func.__name__ = func.__name__
    _func.__doc__ = func.__doc__
    return _func
  
  def __init__ (self, path, edit=False):
    """
    E(path, edit=False)
    
    Create EDFFile object

    Parameters
    ----------
    path : str
        absolute or relative path to .edf file
    edit : bool
        indicates whether the data should be read-only (False) or
        editable (True); set to True when creating a new file
    """
    self.path = path
    self._header = None
    self._data = None
    self._numpy = None
    self._edit = edit
  
  def loadHeader (self, reload=False):
    """
    E.loadHeader(reload=False)
    
    Load header data from file for faster access

    Loading the header is necessary to access records data and annotations
    
    Check full documentation of EDFFile to find the available methods for
    accessing header data
    Note: signal-specific header data must be accessed from a EDFFile.Signal
    object
    
    Parameters
    ----------
    reload : bool
        force loading of header even if it has already been loaded
    """
    if self._header is None or reload:
      self._header = None
      self._data = None
      self._numpy = None
      f = open(self.path, 'rb')
      self._header = f.read(self._GLOBAL_HEADER_STOP)
      self._header += f.read(self.signalsNumber() * self._SIGNAL_HEADER_STOP)
      f.close()
      
      self._signals = [self.Signal(self, x) for x in range(self.signalsNumber())]
      self._annotations = None
      pointer = 0
      for s in self._signals:
        if s.label() == 'EDF Annotations ':
          self._annotations = self.Annotation(s)
          self._annotations._start = pointer
        s._start = pointer
        pointer += 2*s.samplesNumber()
      self._record_size = pointer
  
  def load (self, reload=False):
    """
    E.load(reload=False)
    
    Load records data (and header if not already loaded) from file for faster
    access
    
    Parameters
    ----------
    reload : bool
        force loading of header even if it has already been loaded
    """
    if self._data is None or reload:
      self._data = None
      self._numpy = None
      self.loadHeader(reload)
      f = open(self.path, 'rb')
      f.seek(self.headerSize())
      self._data = f.read()
      f.close()
  
  def np_preload (self):
    """
    E.np_preload()
    
    Load records data (if not already loaded) and convert it to a numpy.ndarray
    accessible with E.numpy()
    
    For more info on the ndarray, check documentation on EDFFile.numpy
    
    Note: it can take a lot of time depending on the size of the file
    """
    if self._numpy is None:
      if self._data is None:
        self.load()
      self._numpy = []
      for s in self._signals:
        self._numpy.append(s[:])
      self._numpy = np.array(self._numpy)
  
  def numpy (self):
    """
    E.numpy()
    
    Return a numpy.ndarray containing all the records data for easier access
    
    The numpy.ndarray must be preloaded with EDFFile.np_preload to access it
    
    The data is organized in three dimensions:
    - signal -- size E.signalsNumber()
    - record -- size E.recordsNumber()
    - sample -- size E.signal(s).samplesNumber() [s: selected signal]
    
    Example
    -------
    E.np_preload()
    E.numpy()[2:5,:10,-1] # take the last sample of the first ten
                          # records of signal channels from 2 to 5
    
    Check documentation on numpy.ndarray for the available methods of the
    returned object
    """
    if self._numpy is None:
      raise Exception("Use EDFFile.np_preload before trying to access the "
      "numpy matrix")
    return self._numpy
    
  def _get_header (self, a, b):
    if self._header is None:
      #~ raise Exception("File header has not been loaded yet")
      f = open(self.path, 'rb')
      f.seek(a)
      ret = f.read(b-a).decode('ascii')
      f.close()
      return ret
    return self._header[a:b].decode('ascii')
  
  def _get_data (self, a, b):
    if self._header is None:
      raise Exception("File header has not been loaded yet; "
      "loading it is necessary before accessing records.")
    if self._data is None:
      f = open(self.path, 'rb')
      f.seek(self.headerSize()+a)
      ret = f.read(b-a)
      f.close()
      return ret
    return self._data[a:b]
  
  def version (self):
    """
    E.version()
    
    Get version of this data format from header
    """
    return self._get_header(*self._VERSION)
  
  def patientId (self):
    """
    E.patientId()
    
    Get local patient identification from header
    """
    return self._get_header(*self._PATIENT_ID)
  
  def recordingId (self):
    """
    E.recordingId()
    
    Get local recording identification from header
    """
    return self._get_header(*self._RECORDING_ID)
  
  def startDate (self):
    """
    E.startDate()
    
    Get startdate of recording (dd.mm.yy) from header
    """
    return self._get_header(*self._START_DATE)
  
  def startTime (self):
    """
    E.startTime()
    
    Get starttime of recording (hh.mm.ss) from header
    """
    return self._get_header(*self._START_TIME)
  
  def headerSize (self):
    """
    E.headerSize()
    
    Get number of bytes in header record from header
    """
    return int(self._get_header(*self._HEADER_SIZE))
  
  def reserved (self):
    """
    E.reserved()
    
    Get reserved block from header
    """
    return self._get_header(*self._RESERVED)
  
  def recordsNumber (self):
    """
    E.recordsNumber()
    
    Get number of data records from header
    """
    return int(self._get_header(*self._RECORDS_NUMBER))
  
  def recordDuration (self):
    """
    E.recordDuration()
    
    Get duration of a data record, in seconds from header
    """
    return int(self._get_header(*self._RECORD_DURATION))
  
  def signalsNumber (self):
    """
    E.signalsNumber()
    
    Get number of signals in data record from header
    """
    return int(self._get_header(*self._SIGNALS_NUMBER))
  
  def signal (self, channel):
    """
    E.signal(channel)
    
    Return a single signal from the file
    
    Negative channels are allowed to start counting from the end (e.g.
    E.signal(-1) returns the last signal available)
    
    For more informations on the returned object, check documentation
    on EDFFile.Signal
    """
    sN = self.signalsNumber()
    if channel < 0:
      channel = sN+channel
    if channel < 0 or channel >= sN:
      raise IndexError("Channel {} not present in file, only {} available".format(channel, sN))
    
    if self._header is None:
      return self.Signal(self, channel)
    else:
      return self._signals[channel]
  
  def annotations (self):
    """
    E.annotations()
    
    Returns the annotations signal contained in the file, if available
    
    For more informations on the returned object, check documentation
    on EDFFile.Annotation
    """
    if self._header is None:
      raise Exception("File header has not been loaded yet; "
      "loading it is necessary before accessing annotations.")
    if self._annotations is None:
      raise Exception("No annotations present in the file")
    return self._annotations
  
  def editable (self, value=None):
    """
    E.editable()      -- get editable status
    E.editable(value) -- set editable status

    Get/set whether the file is editable or not
    """
    if value is None:
      return self._edit
    else:
      self._edit = value
  
  @_edit
  def init (self):
    """
    E.init()
    
    Initialize header and records data for an empty file
    Needed when creating a new file
    
    FOR EDITABLE FILES ONLY
    """
    self._header = bytes(self._GLOBAL_HEADER_STOP)
    self._data = b''
  
  @_edit
  def write (self, path=None):
    """
    E.write([path])

    Write EDF file to disk

    Parameters
    ----------
    path : str
        path of output file; by default the original file path is used

    FOR EDITABLE FILES ONLY
    """
    if self._header is None or self._data is None:
      raise Exception("File has not been properly initialized")
    f = open(path or self.path, 'wb')
    f.write(self._header)
    f.write(self._data)
    f.close()
  
  @_edit
  def removeRecords (self, start, end=None):
    """
    E.removeRecords(start[, end])
    
    Remove one or more consecutive records from the file
    
    Parameters
    ----------
    start : int
        first record to be removed
    end : int
        first record to be kept after the one(s) removed; by default only
        one record is removed
    
    FOR EDITABLE FILES ONLY
    """
    if self._data is None:
      raise Exception("File has not been properly initialized")
    if end is None:
      end = start+1
    if start > end:
      t = start
      start = end
      end = t
    if end > self.recordsNumber():
      raise IndexError("index out of range")
    
    start = self._record_size*start
    end   = self._record_size*end
    self._data = self._data[:start]+self._data[end:]
    self._header = (
      self._header[:self._RECORDS_NUMBER[0]] +
      ("{:<"+str(_RECORDS_NUMBER)+"d}").format(self.recordsNumber()-1).encode('ascii') +
      self._header[self._RECORDS_NUMBER[1]:]
    )
  
  class Signal:
    """
    Class representing a single signal inside a .edf file
    """
    
    def __init__ (self, parent, index):
      """
      Constructor. Internal use only
      """
      self.parent = parent
      self.index = index
    
    def _get_header (self, a, b):
      start = (
        self.parent._GLOBAL_HEADER_STOP +
        self.parent.signalsNumber() * a +
        self.index * b
      )
      return self.parent._get_header(
        start,
        start + b
      )
    
    def _get_data (self, record, sample):
      if self.parent._header is None:
        raise Exception("File header has not been loaded yet; "
        "loading it is necessary before accessing records.")
      
      rN = self.parent.recordsNumber()
      if record < 0:
        record = rN+record
      if record < 0 or record >= rN:
        raise IndexError("Record {} not present in file, only {} available".format(record, rN))
      
      a = self.parent._record_size*record + self._start + 2*sample
      b = a+2
      return int.from_bytes(self.parent._get_data(a, b), byteorder='little', signed=True)
      
    def label (self):
      """
      S.label()
      
      Get label (e.g. EEG Fpz-Cz or Body temp) from header [signal specific]
      """
      return self._get_header(*self.parent._LABEL)

    def transducer (self):
      """
      S.transducer()
      
      Get transducer type (e.g. AgAgCl electrode) from header [signal specific]
      """
      return self._get_header(*self.parent._TRANSDUCER)

    def physDim (self):
      """
      S.physDim()
      
      Get physical dimension (e.g. uV or degreeC) from header [signal specific]
      """
      return self._get_header(*self.parent._PHYS_DIM)

    def physMin (self):
      """
      S.physMin()
      
      Get physical minimum (e.g. -500 or 34) from header [signal specific]
      """
      return float(self._get_header(*self.parent._PHYS_MIN))

    def physMax (self):
      """
      S.physMax()
      
      Get physical maximum (e.g. 500 or 40) from header [signal specific]
      """
      return float(self._get_header(*self.parent._PHYS_MAX))

    def digMin (self):
      """
      S.digMin()
      
      Get digital minimum (e.g. -2048) from header [signal specific]
      """
      return int(self._get_header(*self.parent._DIG_MIN))

    def digMax (self):
      """
      S.digMax()
      
      Get digital maximum (e.g. 2047) from header [signal specific]
      """
      return int(self._get_header(*self.parent._DIG_MAX))

    def prefilt (self):
      """
      S.prefilt()
      
      Get prefiltering (e.g. HP:0.1Hz LP:75Hz) from header [signal specific]
      """
      return self._get_header(*self.parent._PREFILT)

    def samplesNumber (self):
      """
      S.samplesNumber()
      
      Get number of samples in each data record from header [signal specific]
      """
      return int(self._get_header(*self.parent._SAMPLES_NUM))

    def reserved (self):
      """
      S.reserved()
      
      Get reserved block from header [signal specific]
      """
      return self._get_header(*self.parent._RESERVED2)
    
    def __getitem__ (self, i):
      """
      S[record]         -- get full record
      S[record, sample] -- get single sample from record
      
      Access records data from signal
      
      Slices can also be used to access multiple records or multiple samples
      at the same time

      Examples
      --------
      E.signal(0)[3]     # get 3rd record of signal 0 in file E
      E.signal(2)[:, -1] # get last sample of each records of signal 2 in file E
      
      The returned object can either be a single value, a list of values
      (single record), or a list of lists (multiple records)
      Except when a single value is returned, in all other cases a numpy.ndarray
      object is returned
      """
      if isinstance(i, tuple):
        if len(i) > 2:
          raise IndexError("Too many indices")
        record = i[0]
        sample = i[1]
      else:
        record = i
        sample = None
      if isinstance(record, slice):
        rstart = record.start or 0
        rstop  = record.stop or self.parent.recordsNumber()
        rstep  = record.step or 1
      else:
        rstart = record
        rstop  = record+1
        rstep  = 1
      
      if sample is None:
        sample = slice(None, None, None)
      if isinstance(sample, slice):
        sstart = sample.start or 0
        sstop  = sample.stop or self.samplesNumber()
        sstep  = sample.step or 1
      else:
        sstart = sample
        sstop  = sample+1
        sstep  = 1
      ret = []
      for r in range(rstart, rstop, rstep):
        rec = []
        for s in range(sstart, sstop, sstep):
          rec.append(self._get_data(r, s))
        ret.append(rec)
      ret = np.array(ret)
      if ret.shape == (1, 1):
        return ret[0,0]
      if ret.shape[0] == 1:
        return ret[0]
      return ret
  
  class Annotation (Signal):
    """
    Class representing annotations in a .edf file
    """
    def __init__ (self, sig):
      """
      Constructor. Internal use only
      """
      EDFFile.Signal.__init__(self, sig.parent, sig.index)
      self._data = None
    
    def preload (self):
      """
      A.preload()
      
      Preload all annotations for faster access
      """
      data = []
      for record in range(self.parent.recordsNumber()):
        data.append(self._get_data(record))
      self._data = data
    
    def _get_data (self, record):
      rN = self.parent.recordsNumber()
      if record < 0:
        record = rN+record
      if record < 0 or record >= rN:
        raise IndexError("Record {} not present in file, only {} available".format(record, rN))
      
      if self._data is None:
        a = self.parent._record_size*record + self._start
        b = a+2*self.samplesNumber()
        ret = []
        rec = self.parent._get_data(a,b)
        TALs = rec.split(b'\x14\x00')[:-1]
        for tal in TALs:
          tal = tal.split(b'\x14')
          time = tal[0].split(b'\x15')
          events = tal[1:]
          onset = float(time[0].decode('ascii'))
          try:
            duration = float(time[1].decode('ascii'))
          except:
            duration = None
          tal = {
            'onset' : onset,
            'duration' : duration,
            'events' : [e.decode('ascii') for e in events]
          }
          ret.append(tal)
        return ret
      else:
        return self._data[record]
    
    def __getitem__ (self, record):
      """
      A[record]
      
      Return all annotations in a record (a slice can also be used to access
      multiple records at the same time)
      
      For each selected record, a list of annotations is returned
      Each annotation is a dictionary with keys:
      * 'onset'    -- amount of seconds by which the onset of the annotated
                      event follows ('+') or precedes ('-') the startdate/time
                      of the file
      * 'duration' -- duration of the annotated event in seconds (can be None)
      * 'events'   -- list of annotations all sharing the same onset and
                      duration
      
      For more info on EDF+ annotations, check the official specification
      http://www.edfplus.info/specs/edfplus.html#edfplusannotations
      """
      if isinstance(record, tuple):
        raise IndexError("Too many indices")
      if isinstance(record, slice):
        rstart = record.start or 0
        rstop  = record.stop or self.parent.recordsNumber()
        rstep  = record.step or 1
      else:
        rstart = record
        rstop  = record+1
        rstep  = 1
      ret = []
      for r in range(rstart, rstop, rstep):
        ret.append(self._get_data(r))
      if len(ret) == 1:
        ret = ret[0]
      return ret

_VERSION = 8
_PATIENT_ID = 80
_RECORDING_ID = 80
_START_DATE = 8
_START_TIME = 8
_HEADER_SIZE = 8
_RESERVED = 44
_RECORDS_NUMBER = 8
_RECORD_DURATION = 8
_SIGNALS_NUMBER = 4

_LABEL = 16
_TRANSDUCER = 80
_PHYS_DIM = 8
_PHYS_MIN = 8
_PHYS_MAX = 8
_DIG_MIN = 8
_DIG_MAX = 8
_PREFILT = 80
_SAMPLES_NUM = 8
_RESERVED2 = 32
def _init():
  a = 0
  b = a + _VERSION
  EDFFile._VERSION = (a,b)
  a = b
  b = a + _PATIENT_ID
  EDFFile._PATIENT_ID = (a,b)
  a = b
  b = a + _RECORDING_ID
  EDFFile._RECORDING_ID = (a,b)
  a = b
  b = a + _START_DATE
  EDFFile._START_DATE = (a,b)
  a = b
  b = a + _START_TIME
  EDFFile._START_TIME = (a,b)
  a = b
  b = a + _HEADER_SIZE
  EDFFile._HEADER_SIZE = (a,b)
  a = b
  b = a + _RESERVED
  EDFFile._RESERVED = (a,b)
  a = b
  b = a + _RECORDS_NUMBER
  EDFFile._RECORDS_NUMBER = (a,b)
  a = b
  b = a + _RECORD_DURATION
  EDFFile._RECORD_DURATION = (a,b)
  a = b
  b = a + _SIGNALS_NUMBER
  EDFFile._SIGNALS_NUMBER = (a,b)
  EDFFile._GLOBAL_HEADER_STOP = b

  a = 0
  EDFFile._LABEL = (a,_LABEL)
  a += _LABEL
  EDFFile._TRANSDUCER = (a,_TRANSDUCER)
  a += _TRANSDUCER
  EDFFile._PHYS_DIM = (a,_PHYS_DIM)
  a += _PHYS_DIM
  EDFFile._PHYS_MIN = (a,_PHYS_MIN)
  a += _PHYS_MIN
  EDFFile._PHYS_MAX = (a,_PHYS_MAX)
  a += _PHYS_MAX
  EDFFile._DIG_MIN = (a,_DIG_MIN)
  a += _DIG_MIN
  EDFFile._DIG_MAX = (a,_DIG_MAX)
  a += _DIG_MAX
  EDFFile._PREFILT = (a,_PREFILT)
  a += _PREFILT
  EDFFile._SAMPLES_NUM = (a,_SAMPLES_NUM)
  a += _SAMPLES_NUM
  EDFFile._RESERVED2 = (a,_RESERVED2)
  a += _RESERVED2
  EDFFile._SIGNAL_HEADER_STOP = a

_init()
